﻿using System.Threading.Tasks;

namespace Hunabku.Storage.Queues
{
	public interface IQueueMessageConsumer<in TMessage> where TMessage : class
	{
		Task ProcessMessage(TMessage message);
	}
}
﻿using System.Threading.Tasks;

namespace Hunabku.Storage.Queues
{
	public interface IDequeuer<TMessage> where TMessage : class
	{
		Task<TMessage> Dequeue(int? timeoutMilliseconds = null);
	}
}
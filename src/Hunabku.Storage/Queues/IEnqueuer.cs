﻿using System;
using System.Threading.Tasks;

namespace Hunabku.Storage.Queues
{
	public interface IEnqueuer<in TMessage> where TMessage : class
	{
		Task Enqueue(TMessage message, TimeSpan? visibleIn = null);
	}
}
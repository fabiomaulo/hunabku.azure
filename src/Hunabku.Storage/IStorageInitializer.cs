﻿namespace Hunabku.Storage
{
	public interface IStorageInitializer
	{
		void Initialize();
		void Drop();
	}
}
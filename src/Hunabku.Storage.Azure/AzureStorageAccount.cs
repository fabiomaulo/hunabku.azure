﻿using System;
using System.Linq;
using Microsoft.WindowsAzure.Storage;

namespace Hunabku.Storage.Azure
{
#if !NETSTANDARD
	public class AzureStorageAccount
	{
		public static CloudStorageAccount GetAccount(params string[] connectionStringKey)
		{
			return CloudStorageAccount.Parse(RequiredConnectionString(connectionStringKey));
		}

		public static string RequiredConnectionString(params string[] connectionStringKey)
		{
			var connectionString = connectionStringKey.Select(GetConnectionString).FirstOrDefault(x => x != null);
			if (string.IsNullOrEmpty(connectionString))
			{
				if (connectionStringKey.Length == 1)
				{
					throw new Exception($"The Azure Cloud connection string configuration is required;\nCheck your config file for the key {connectionStringKey[0]}.");
				}
				throw new Exception($"The Azure Cloud connection string configuration is required;\nCheck your config file for one of the keys {string.Join(", ", connectionStringKey)}.");
			}
			return connectionString;
		}

		private static string GetConnectionString(string connectionStringKey)
		{
			var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connectionStringKey]?.ConnectionString;
			if (string.IsNullOrEmpty(connectionString))
			{
				connectionString = System.Configuration.ConfigurationManager.AppSettings[connectionStringKey];
			}
			return connectionString;
		}
	}
#endif
#if NETSTANDARD
	public class AzureStorageAccount
	{
		public static CloudStorageAccount GetAccount(Microsoft.Extensions.Configuration.IConfigurationRoot configuration, params string[] connectionStringKey)
		{
			return CloudStorageAccount.Parse(RequiredConnectionString(configuration, connectionStringKey));
		}

		public static string RequiredConnectionString(Microsoft.Extensions.Configuration.IConfigurationRoot configuration, params string[] connectionStringKey)
		{
			var connectionString = connectionStringKey.Select(x=> configuration[x]).FirstOrDefault(x => x != null);
			if (string.IsNullOrEmpty(connectionString))
			{
				if (connectionStringKey.Length == 1)
				{
					throw new Exception($"The Azure Cloud connection string configuration is required;\nCheck your config file for the key {connectionStringKey[0]}.");
				}
				throw new Exception($"The Azure Cloud connection string configuration is required;\nCheck your config file for one of the keys {string.Join(", ", connectionStringKey)}.");
			}
			return connectionString;
		}
	}
#endif
}
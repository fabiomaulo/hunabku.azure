﻿using System;
using System.Linq;

namespace Hunabku.Storage.Azure.Tables
{
	public class TableNaming
	{
		private static readonly string[] tableEntityTypePostfixSequence = { "DataRow", "Data", "TableEntity", "Entity" };

		public static string AsTableStorageName<TMessage>() where TMessage : class
		{
			return AsTableStorageName(typeof(TMessage));
		}

		public static string AsTableStorageName(Type dataRowType)
		{
			var typeName = dataRowType.Name;
			return ConvertToTableName(tableEntityTypePostfixSequence
				.Select(x => TrucateTableName(x, typeName))
				.FirstOrDefault(x => x != null) ?? typeName);
		}

		private static string TrucateTableName(string postfix, string typeName)
		{
			return typeName.EndsWith(postfix) ? typeName.Substring(0, typeName.Length - postfix.Length) : null;
		}

		public static string ConvertToTableName(string candidateName)
		{
			if (string.IsNullOrWhiteSpace(candidateName))
			{
				return null;
			}
			/* TableName 3-63	case-insensitive alphanumeric */
			return new string(candidateName.Where(char.IsLetterOrDigit).Select(char.ToLowerInvariant).ToArray());
		}
	}
}
﻿using System;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Hunabku.Storage;

namespace Hunabku.Storage.Azure.Tables
{
	public class TableStorageInitializer : IStorageInitializer
	{
		private readonly CloudStorageAccount account;
		private readonly string entityTableName;

		public TableStorageInitializer(CloudStorageAccount account, string entityTableName)
		{
			this.account = account ?? throw new ArgumentNullException(nameof(account));
			this.entityTableName = entityTableName ?? throw new ArgumentNullException(nameof(entityTableName));
		}

		public void Initialize()
		{
			var tableClient = account.CreateCloudTableClient();
			var table = tableClient.GetTableReference(entityTableName);
			table.CreateIfNotExistsAsync().Wait();
		}

		public void Drop()
		{
			var tableClient = account.CreateCloudTableClient();
			var table = tableClient.GetTableReference(entityTableName);
			table.DeleteIfExistsAsync().Wait();
		}
	}

	public class TableStorageInitializer<TTableEntity> : TableStorageInitializer where TTableEntity : ITableEntity
	{
		public TableStorageInitializer(CloudStorageAccount account)
			: base(account, typeof(TTableEntity).AsTableStorageName()) {}
	}
}
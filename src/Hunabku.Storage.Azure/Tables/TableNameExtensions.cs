﻿using System;
using System.Linq;
using Microsoft.WindowsAzure.Storage.Table;

namespace Hunabku.Storage.Azure.Tables
{
	public static class TableNameExtensions
	{
		private static readonly string[] tableEntityTypePostfixSequence = {"DataRow", "Data", "TableEntity", "Entity"};
		public static string AsTableStorageName(this Type dataRowType)
		{
			var typeName = dataRowType.Name;
			return ConvertToTableName(tableEntityTypePostfixSequence
				.Select(x => TrucateTableName(x, typeName))
				.FirstOrDefault(x => x != null) ?? typeName);
		}

		private static string TrucateTableName(string postfix, string typeName)
		{
			return typeName.EndsWith(postfix) ? typeName.Substring(0, typeName.Length - postfix.Length) : null;
		}

		public static string ConvertToTableName(string candidateName)
		{
			if (string.IsNullOrWhiteSpace(candidateName))
			{
				return null;
			}
			/* TableName 3-63	case-insensitive	alphanumeric */
			return new string(candidateName.Where(char.IsLetterOrDigit).Select(char.ToLowerInvariant).ToArray());
		}

		/// <summary>
		/// Get the <see cref="EntityProperty"/> if available.
		/// </summary>
		/// <param name="source">The <see cref="DynamicTableEntity"/> instance.</param>
		/// <param name="nameOfTableField">The name of the field.</param>
		/// <returns>
		/// The <see cref="EntityProperty"/> if the field is available, or null when the <see cref="DynamicTableEntity"/> does not have the property.
		/// </returns>
		public static EntityProperty Get(this DynamicTableEntity source, string nameOfTableField)
		{
			if (source == null)
			{
				throw new ArgumentNullException(nameof(source));
			}
			if (string.IsNullOrWhiteSpace(nameOfTableField))
			{
				throw new ArgumentNullException(nameof(nameOfTableField));
			}
			EntityProperty field;
			if (source.Properties.TryGetValue(nameOfTableField, out field))
			{
				return field;
			}
			return null;
		}

		public static void Safely(this DynamicTableEntity source, string nameOfTableField, Action<EntityProperty> setter)
		{
			if (setter == null)
			{
				throw new ArgumentNullException(nameof(setter));
			}
			var tableProperty = Get(source, nameOfTableField);
			if (tableProperty != null)
			{
				setter(tableProperty);
			}
		}
	}
}
﻿using System;
using Microsoft.ServiceBus;

namespace Hunabku.Storage.Azure.Queues
{
	/// <summary>
	///   Initialize a servicebus queue specific for a message type.
	/// </summary>
	/// <typeparam name="TMessage">The type of the message</typeparam>
	public class ServiceBusQueueInitializer<TMessage> : IStorageInitializer where TMessage : class
	{
		private readonly string queueName = GetName();
		private readonly string serviceBusConnectionString;

		public ServiceBusQueueInitializer(string serviceBusConnectionString)
		{
			if (string.IsNullOrWhiteSpace(serviceBusConnectionString))
			{
				throw new ArgumentNullException(nameof(serviceBusConnectionString));
			}
			this.serviceBusConnectionString = serviceBusConnectionString;
		}

		public void Initialize()
		{
			var namespaceManager = NamespaceManager.CreateFromConnectionString(serviceBusConnectionString);
			if (!namespaceManager.QueueExists(queueName))
			{
				namespaceManager.CreateQueue(queueName);
			}
		}

		public void Drop()
		{
			var namespaceManager = NamespaceManager.CreateFromConnectionString(serviceBusConnectionString);
			if (namespaceManager.QueueExists(queueName))
			{
				namespaceManager.DeleteQueue(queueName);
			}
		}

		public static string GetName() => QueueNaming.AsQueueStorageName<TMessage>();
	}
}
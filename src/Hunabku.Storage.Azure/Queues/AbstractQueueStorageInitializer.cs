﻿using System;
using Microsoft.WindowsAzure.Storage;
using Hunabku.Storage;

namespace Hunabku.Storage.Azure.Queues
{
	public abstract class AbstractQueueStorageInitializer: IStorageInitializer
	{
		private readonly CloudStorageAccount account;

		protected AbstractQueueStorageInitializer(CloudStorageAccount account)
		{
			if (account == null)
			{
				throw new ArgumentNullException(nameof(account));
			}
			this.account = account;
		}

		public void Initialize()
		{
			var queueClient = account.CreateCloudQueueClient();
			var queue = queueClient.GetQueueReference(GetQueueName());
			queue.CreateIfNotExistsAsync().Wait();
		}

		public void Drop()
		{
			var queueClient = account.CreateCloudQueueClient();
			var queue = queueClient.GetQueueReference(GetQueueName());
			queue.DeleteIfExistsAsync().Wait();
		}

		protected abstract string GetQueueName();
	}
}
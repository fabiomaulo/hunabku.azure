﻿using System;
using System.Linq;

namespace Hunabku.Storage.Azure.Queues
{
	public class QueueNaming
	{
		private static readonly string[] queueMessageTypePostfixSequence = { "EventMessage", "Message", "Event" };

		public static string AsQueueStorageName<TMessage>() where TMessage : class
		{
			return AsQueueStorageName(typeof(TMessage));
		}

		public static string AsQueueStorageName(Type messageType)
		{
			var typeName = messageType.Name;
			return FixToQueueName(queueMessageTypePostfixSequence
				.Select(x => TrucateQueueName(x, typeName))
				.FirstOrDefault(x => x != null) ?? typeName);
		}

		private static string TrucateQueueName(string postfix, string typeName)
		{
			return typeName.EndsWith(postfix) ? typeName.Substring(0, typeName.Length - postfix.Length) : null;
		}

		public static string FixToQueueName(string candidateName)
		{
			if (string.IsNullOrWhiteSpace(candidateName))
			{
				return null;
			}
			/* QueueName 3-63 lowercase alphanumeric and dash */
			return new string(candidateName.Where(x => char.IsLetterOrDigit(x) || x == '-').Select(char.ToLowerInvariant).ToArray());
		}
	}
}
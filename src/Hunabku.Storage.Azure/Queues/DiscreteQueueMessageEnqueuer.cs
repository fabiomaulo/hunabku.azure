﻿using System;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using Hunabku.Storage.Queues;

namespace Hunabku.Storage.Azure.Queues
{
	/// <summary>
	/// Enqueuer of JSON messages.
	/// </summary>
	/// <typeparam name="TMessage">The class representing the message</typeparam>
	public class DiscreteQueueMessageEnqueuer<TMessage> : IEnqueuer<TMessage> where TMessage : class
	{
		private readonly CloudQueue queueRef;

		public DiscreteQueueMessageEnqueuer(CloudStorageAccount account, string queueName)
		{
			if (account == null)
			{
				throw new ArgumentNullException(nameof(account));
			}
			queueName = string.IsNullOrWhiteSpace(queueName) ? QueueNaming.AsQueueStorageName<TMessage>(): QueueNaming.FixToQueueName(queueName);
			var queueClient = account.CreateCloudQueueClient();
			queueRef = queueClient.GetQueueReference(queueName);
			queueRef.CreateIfNotExistsAsync().Wait();
		}

		public Task Enqueue(TMessage message, TimeSpan? visibleIn = null)
		{
			if (message == null)
			{
				throw new ArgumentNullException(nameof(message));
			}
			var serializedMessage = JsonConvert.SerializeObject(message);
			var qmessage = new CloudQueueMessage(serializedMessage);
			return queueRef.AddMessageAsync(qmessage, null, visibleIn, null, null);
		}
	}
}
﻿using Microsoft.WindowsAzure.Storage;

namespace Hunabku.Storage.Azure.Queues
{
	/// <summary>
	///   Initialize a queue storage specific for a message type.
	/// </summary>
	/// <typeparam name="TMessage">The type of the message</typeparam>
	public class QueueStorageInitializer<TMessage> : AbstractQueueStorageInitializer where TMessage : class
	{
		private readonly string queueName = GetName();

		public QueueStorageInitializer(CloudStorageAccount account) : base(account) {}

		public static string GetName() => QueueNaming.AsQueueStorageName<TMessage>();

		protected override string GetQueueName() => queueName;
	}
}
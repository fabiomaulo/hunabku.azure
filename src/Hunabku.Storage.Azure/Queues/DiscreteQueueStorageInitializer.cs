﻿using Microsoft.WindowsAzure.Storage;

namespace Hunabku.Storage.Azure.Queues
{
	public class DiscreteQueueStorageInitializer: AbstractQueueStorageInitializer
	{
		private readonly string queueName;
		public DiscreteQueueStorageInitializer(CloudStorageAccount account, string queueName) : base(account)
		{
			this.queueName = queueName;
		}
		protected override string GetQueueName() => queueName;
	}
}
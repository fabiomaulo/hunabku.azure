﻿using System;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using Hunabku.Storage.Queues;

namespace Hunabku.Storage.Azure.Queues
{
	public class QueueMessageDequeuer<TMessage> : IDequeuer<TMessage> where TMessage : class
	{
		private readonly string queueName = QueueNaming.AsQueueStorageName<TMessage>();
		private readonly CloudQueue queueRef;

		public QueueMessageDequeuer(CloudStorageAccount account)
		{
			if (account == null)
			{
				throw new ArgumentNullException(nameof(account));
			}
			var queueClient = account.CreateCloudQueueClient();
			queueRef = queueClient.GetQueueReference(queueName);
		}

		public async Task<TMessage> Dequeue(int? timeoutMilliseconds = null)
		{
			var queueMessage = timeoutMilliseconds.HasValue ? queueRef.GetMessageAsync(TimeSpan.FromMilliseconds(timeoutMilliseconds.Value), null, null):queueRef.GetMessageAsync();
			if (queueMessage == null)
			{
				return null;
			}
			var messageContent = (await queueMessage).AsString;
			var message = JsonConvert.DeserializeObject<TMessage>(messageContent);
			return message;
		}
	}
}
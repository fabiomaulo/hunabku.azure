﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Hunabku.Storage.Queues;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

namespace Hunabku.Storage.Azure.Queues
{
	public class ServiceBusEnqueuer<TMessage> : IEnqueuer<TMessage> where TMessage : class
	{
		private readonly string queueName = QueueNaming.AsQueueStorageName<TMessage>();
		private readonly string serviceBusConnectionString;

		public ServiceBusEnqueuer(string serviceBusConnectionString)
		{
			if (string.IsNullOrWhiteSpace(serviceBusConnectionString))
			{
				throw new ArgumentNullException(nameof(serviceBusConnectionString));
			}
			this.serviceBusConnectionString = serviceBusConnectionString;
		}

		public async Task Enqueue(TMessage message, TimeSpan? visibleIn = null)
		{
			var client = QueueClient.CreateFromConnectionString(serviceBusConnectionString, queueName);
			var serializedObject = SerializeObjectAsString(message);
			using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(serializedObject), false))
			{
				var bmessage = new BrokeredMessage(stream)
				{
					ContentType = "application/json",
				};
				if(visibleIn.HasValue)
				{
					bmessage.ScheduledEnqueueTimeUtc = DateTime.UtcNow.Add(visibleIn.Value);
				}
				await client.SendAsync(bmessage);
			}
		}

		protected virtual string SerializeObjectAsString(TMessage message)
		{
			return JsonConvert.SerializeObject(message);
		}
	}
}
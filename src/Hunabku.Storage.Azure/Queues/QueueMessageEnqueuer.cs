﻿using System;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using Hunabku.Storage.Queues;

namespace Hunabku.Storage.Azure.Queues
{
	/// <summary>
	/// Enqueuer of JSON messages.
	/// </summary>
	/// <typeparam name="TMessage">The class representing the message</typeparam>
	public class QueueMessageEnqueuer<TMessage> : IEnqueuer<TMessage> where TMessage : class
	{
		private readonly CloudStorageAccount account;
		private readonly string queueName = QueueNaming.AsQueueStorageName<TMessage>();

		public QueueMessageEnqueuer(CloudStorageAccount account)
		{
			this.account = account ?? throw new ArgumentNullException(nameof(account));
			var queueClient = account.CreateCloudQueueClient();
			var queueRef = queueClient.GetQueueReference(queueName);
			queueRef.CreateIfNotExistsAsync().Wait();
		}

		public Task Enqueue(TMessage message, TimeSpan? visibleIn = null)
		{
			if (message == null)
			{
				throw new ArgumentNullException(nameof(message));
			}
			var queueClient = account.CreateCloudQueueClient();
			var queueRef = queueClient.GetQueueReference(queueName);
			var serializedMessage = JsonConvert.SerializeObject(message);
			var qmessage = new CloudQueueMessage(serializedMessage);
			return queueRef.AddMessageAsync(qmessage, null, visibleIn, null, null);
		}
	}
}
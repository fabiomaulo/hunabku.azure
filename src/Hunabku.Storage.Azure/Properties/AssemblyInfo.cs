﻿using System.Reflection;

[assembly: AssemblyTitle("Hunabku.Storage.Azure")]
[assembly: AssemblyDescription("Implementations to work with Azure")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Hunabku.Storage.Azure")]
[assembly: AssemblyCopyright("Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

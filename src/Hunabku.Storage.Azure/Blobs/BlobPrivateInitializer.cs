﻿using System;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Hunabku.Storage;

namespace Hunabku.Storage.Azure.Blobs
{
	public class BlobPrivateInitializer : IStorageInitializer
	{
		private readonly CloudStorageAccount account;
		private readonly string containerName;

		public BlobPrivateInitializer(CloudStorageAccount account, string containerName)
		{
			if (account == null)
			{
				throw new ArgumentNullException(nameof(account));
			}
			if (string.IsNullOrWhiteSpace(containerName))
			{
				throw new ArgumentNullException(nameof(containerName));
			}
			this.account = account;
			this.containerName = BlobNaming.FixToContainerName(containerName);
		}

		public void Initialize()
		{
			CloudBlobClient blobStorageType = account.CreateCloudBlobClient();
			CloudBlobContainer container = blobStorageType.GetContainerReference(containerName);
			container.CreateIfNotExistsAsync().Wait();
			var perm = new BlobContainerPermissions
			{
				PublicAccess = BlobContainerPublicAccessType.Off
			};
			container.SetPermissionsAsync(perm).Wait();
		}

		public void Drop()
		{
			CloudBlobClient blobStorageType = account.CreateCloudBlobClient();
			CloudBlobContainer container = blobStorageType.GetContainerReference(containerName);
			container.DeleteIfExistsAsync().Wait();
		}
	}
}
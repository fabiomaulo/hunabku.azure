﻿using System.Linq;

namespace Hunabku.Storage.Azure.Blobs
{
	public static class BlobNaming
	{
		public static string FixToContainerName(string candidateName)
		{
			if (string.IsNullOrWhiteSpace(candidateName))
			{
				return null;
			}
			/* ContainerName 3-63 lowercase alphanumeric and dash */
			return new string(candidateName.Where(x => char.IsLetterOrDigit(x) || (x == '-')).Select(char.ToLowerInvariant).ToArray());
		}
	}
}